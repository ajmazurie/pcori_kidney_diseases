
\echo '    staging events for covariates #4*'

\echo '      creating _drugs_during_periods_1_and_2'

DROP TABLE IF EXISTS _drugs_during_periods_1_and_2 CASCADE;
CREATE TEMPORARY TABLE _drugs_during_periods_1_and_2 AS
  SELECT DISTINCT
         index_event.person_id,
         event.concept_set_name
    FROM sequences_of_interest AS index_event,
         events_of_interest AS event
   WHERE (index_event.person_id = event.person_id)
     AND (event.concept_set_name LIKE 'drugs;concomitant;%')
     AND (lower(event.event_period) >= lower(index_event.period_1))
     AND (lower(event.event_period) < upper(index_event.period_2) - 1);

CREATE INDEX ON _drugs_during_periods_1_and_2 (person_id);
CREATE INDEX ON _drugs_during_periods_1_and_2 (concept_set_name);
CREATE INDEX ON _drugs_during_periods_1_and_2 (concept_set_name VARCHAR_PATTERN_OPS);

\echo '    calculating covariate #4(a), `exposure_to_opioid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_opioid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;analgesic;opioid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(b), `exposure_to_heroin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_heroin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;analgesic;opioid;heroin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(c), `exposure_to_nsaid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_nsaid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;nsaid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(d), `exposure_to_non_narcotic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_non_narcotic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;analgesic;non_narcotic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(e), `exposure_to_sedative_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_sedative_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;hypnotic_sedativ;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(f), `exposure_to_antianxiety_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antianxiety_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_anxiety;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(g), `exposure_to_benzodiazepine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_benzodiazepine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;benzodiazepine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(h), `exposure_to_anticonvulsant_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anticonvulsant_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_convulsant;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(i), `exposure_to_phenytoin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_phenytoin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_convulsant;phenytoin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(j), `exposure_to_antibacterial_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antibacterial_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(k), `exposure_to_penicillin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_penicillin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;penicillin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(l), `exposure_to_cephalosporin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cephalosporin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;cephalosporin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(m), `exposure_to_fluoroquinolon_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_fluoroquinolon_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;fluoroquinolon;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(n), `exposure_to_tetracyclin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_tetracyclin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;tetracyclin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(o), `exposure_to_aminoglycosid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_aminoglycosid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;aminoglycosid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(p), `exposure_to_macrolid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_macrolid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;macrolid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(q), `exposure_to_carbapenem_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_carbapenem_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;carbapenem;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(r), `exposure_to_amphotericin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_amphotericin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;amphotericin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(s), `exposure_to_vancomycin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_vancomycin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;vancomycin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(t), `exposure_to_rifampin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_rifampin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;rifampin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(u), `exposure_to_ciprofloxacin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_ciprofloxacin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;ciprofloxacin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(v), `exposure_to_demeclocyclin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_demeclocyclin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_bacterial;demeclocyclin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(w), `exposure_to_antiviral_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antiviral_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_viral;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(x), `exposure_to_foscarnet_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_foscarnet_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_viral;foscarnet;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(y), `exposure_to_anti_hiv_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anti_hiv_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_hiv;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(z), `exposure_to_antitubercular_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antitubercular_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_tubercular;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(aa), `exposure_to_ace_inhibitor_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_ace_inhibitor_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;ace_inhibitor;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(bb), `exposure_to_diuretic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_diuretic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;diuretic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(cc), `exposure_to_loop_diuretic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_loop_diuretic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;loop_diuretic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(dd), `exposure_to_thiazide_diuretic_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_thiazide_diuretic_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;thiazid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ee), `exposure_to_mannitol_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_mannitol_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;diuretic;mannitol;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ff), `exposure_to_furosemide_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_furosemide_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;diuretic;furosemide;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(gg), `exposure_to_beta_blocker_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_beta_blocker_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;beta_blocker;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(hh), `exposure_to_anti_arrhythmia_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anti_arrhythmia_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_arrhytmic;%') -- note the typo here
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ii), `exposure_to_calcium_channel_blocker_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_calcium_channel_blocker_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;calcium_channel;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(jj), `exposure_to_cns_stimulant_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cns_stimulant_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;cns_stimulant;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(kk), `exposure_to_alcohol_deterrent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_alcohol_deterrent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;alcohol_deterrent;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ll), `exposure_to_narcotic_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_narcotic_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;narcotic_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(mm), `exposure_to_hormone_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_hormone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;hormone;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(nn), `exposure_to_androgen_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_androgen_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;androgen_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(oo), `exposure_to_estrogen_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_estrogen_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;estrogen_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(pp), `exposure_to_estrogen_receptor_modulator_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_estrogen_receptor_modulator_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;estrogen_receptor_modulator;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(qq), `exposure_to_prostaglandin_antagonist_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_prostaglandin_antagonist_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;prostaglandin_antagonist;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(rr), `exposure_to_androgen_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_androgen_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;androgen;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ss), `exposure_to_estrogen_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_estrogen_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;estrogen;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(tt), `exposure_to_progestin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_progestin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;progestin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(uu), `exposure_to_reproductive_control_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_reproductive_control_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;reproductive_control;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(vv), `exposure_to_glucocorticoid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_glucocorticoid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;glucocorticoid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ww), `exposure_to_thyroid_hormone_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_thyroid_hormone_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;thyroid_hormone;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(xx), `exposure_to_anti_thyroid_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_anti_thyroid_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_thyroid;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(yy), `exposure_to_p450_inducer_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_p450_inducer_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;cytochrome_p450_inducer;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(zz), `exposure_to_p450_inhibitor_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_p450_inhibitor_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;cytochrome_p450_inhibitor;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(aaa), `exposure_to_antihistamine_h1_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antihistamine_h1_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_histamine_h1;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(bbb), `exposure_to_vasodilator_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_vasodilator_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;vasodilators;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ccc), `exposure_to_immunoglobulin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_immunoglobulin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;immunoglobulins;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ddd), `exposure_to_contrast_media_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_contrast_media_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;contrast_media;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(eee), `exposure_to_antineoplastic_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antineoplastic_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;antineoplastic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(fff), `exposure_to_cisplatin_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cisplatin_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;antineoplastic;cisplatin;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ggg), `exposure_to_cyclophosphamide_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cyclophosphamide_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;antineoplastic;cyclophosphamide;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(hhh), `exposure_to_immunosuppressive_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_immunosuppressive_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;immunosuppressive;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(iii), `exposure_to_cyclosporine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cyclosporine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;immunosuppressive;cyclosporine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(jjj), `exposure_to_beta_lactamase_inhibitor_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_beta_lactamase_inhibitor_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;beta_lactamase_inhibitors;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(kkk), `exposure_to_sulfonylurea_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_sulfonylurea_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;sulfonylurea;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(lll), `exposure_to_antihistamine_h2_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_antihistamine_h2_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_histamine_h2;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(mmm), `exposure_to_cimetidine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_cimetidine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_histamine_h2;cimetidine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(nnn), `exposure_to_ranitidine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_ranitidine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;anti_histamine_h2;ranitidine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ooo), `exposure_to_bone_density_conservation_agent_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_bone_density_conservation_agent_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;bone_density_conservation;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ppp), `exposure_to_pamidronate_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_pamidronate_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;pamidronate;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(qqq), `exposure_to_gold_preparation_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_gold_preparation_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;gold_preparation;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(rrr), `exposure_to_penicillamine_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_penicillamine_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;penicillamine;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(sss), `exposure_to_fluoride_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_fluoride_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;fluoride;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(ttt), `exposure_to_aristolochic_acid_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_aristolochic_acid_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;aristolochic;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;

\echo '    calculating covariate #4(uuu), `exposure_to_sulfa_drug_during_period_1_or_2`'

INSERT INTO variables
  SELECT op_start.person_id,
         'exposure_to_sulfa_drug_during_period_1_or_2'::TEXT,
         (EXISTS (
           SELECT TRUE
             FROM _drugs_during_periods_1_and_2 AS drug_exposure
            WHERE (drug_exposure.person_id = op_start.person_id)
              AND (drug_exposure.concept_set_name LIKE
                    'drugs;concomitant;sulfa_drugs;%')
           ))::INTEGER
    FROM sequences_of_interest AS op_start;
