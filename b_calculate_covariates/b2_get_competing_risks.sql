
SET search_path TO {{work_schema}};

\echo '* creating {{work_schema}}.competing_risks'

DROP TABLE IF EXISTS competing_risks CASCADE;
CREATE TABLE competing_risks
  (
      person_id BIGINT NOT NULL,
   event_period DATERANGE NOT NULL,
     event_type TEXT NOT NULL
  );

COMMENT ON TABLE competing_risks IS '{{pipeline_version}}';

\echo '  listing competing risk #1, `moderate_kidney_disorder_diagnosis`'

CREATE TEMPORARY TABLE _first_metavisit_with_moderate_kidney_disorder AS
    SELECT DISTINCT ON (person_id)
           metavisit.*
      FROM metavisits_of_interest AS metavisit
      JOIN sequences_of_interest AS sequence
     USING (person_id)
     WHERE (lower(metavisit.metavisit_period) > sequence.period_3)
       AND (EXISTS (
            SELECT true
              FROM events_of_interest AS event
             WHERE (event.metavisit_occurrence_id
                     = metavisit.metavisit_occurrence_id)
               AND (
                       (event.concept_set_name LIKE
                         'diagnoses;kidney_disorder;diagnosis;moderate;%')
                    OR (event.concept_set_name LIKE
                         'procedures;kidney_disorder;procedure;chronic;%')
                    OR (event.concept_set_name LIKE
                         'procedures;kidney_disorder;procedure;calculus;%')
                   )
           ))
       AND (NOT EXISTS (
            SELECT true
              FROM events_of_interest AS event
             WHERE (event.metavisit_occurrence_id
                     = metavisit.metavisit_occurrence_id)
               AND (event.concept_set_name LIKE
                     'diagnoses;kidney_disorder;diagnosis;severe;%')
           ))
  ORDER BY person_id,
           lower(metavisit_period);

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'moderate_kidney_disorder_diagnosis'::TEXT
    FROM _first_metavisit_with_moderate_kidney_disorder AS metavisit
   WHERE (NOT EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (event.concept_set_name LIKE
                   'diagnoses;exclusion;kidney;%')
         ));

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'censoring_event'::TEXT
    FROM _first_metavisit_with_moderate_kidney_disorder AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (event.concept_set_name LIKE
                   'diagnoses;exclusion;kidney;%')
         ));

\echo '  listing competing risk #2, `severe_kidney_disorder_diagnosis`'

CREATE TEMPORARY TABLE _first_metavisit_with_severe_kidney_disorder AS
    SELECT DISTINCT ON (person_id)
           metavisit.*
      FROM metavisits_of_interest AS metavisit
      JOIN sequences_of_interest AS sequence
     USING (person_id)
     WHERE (lower(metavisit.metavisit_period) > sequence.period_3)
       AND (EXISTS (
            SELECT true
              FROM events_of_interest AS event
             WHERE (event.metavisit_occurrence_id
                     = metavisit.metavisit_occurrence_id)
               AND (
                       (event.concept_set_name LIKE
                         'diagnoses;kidney_disorder;diagnosis;severe;%')
                    OR (event.concept_set_name LIKE
                         'procedures;kidney_disorder;procedure;esrd;%')
                    OR (event.concept_set_name LIKE
                         'procedures;kidney_disorder;procedure;transplant;%')
                   )
           ))
  ORDER BY person_id,
           lower(metavisit_period);

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'severe_kidney_disorder_diagnosis'::TEXT
    FROM _first_metavisit_with_severe_kidney_disorder AS metavisit
   WHERE (NOT EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (event.concept_set_name LIKE
                   'diagnoses;exclusion;kidney;%')
         ));

INSERT INTO competing_risks
  SELECT person_id,
         metavisit_period,
         'censoring_event'::TEXT
    FROM _first_metavisit_with_severe_kidney_disorder AS metavisit
   WHERE (EXISTS (
          SELECT true
            FROM events_of_interest AS event
           WHERE (event.metavisit_occurrence_id
                   = metavisit.metavisit_occurrence_id)
             AND (event.concept_set_name LIKE
                   'diagnoses;exclusion;kidney;%')
         ));

\echo '  listing competing risk #3, `hospitalization_without_kidney_disorder`'

INSERT INTO competing_risks
  WITH
    _first_metavisit_without_diagnosis AS (
         SELECT DISTINCT ON (person_id)
                person_id,
                metavisit_period
           FROM metavisits_of_interest AS metavisit
           JOIN sequences_of_interest AS sequence
          USING (person_id)
          WHERE (metavisit.visit_types && '{inpatient,emergency}'::TEXT[])
            AND (lower(metavisit.metavisit_period) > sequence.period_3)
            AND (NOT EXISTS (
                 SELECT true
                   FROM events_of_interest AS event
                  WHERE (event.metavisit_occurrence_id
                          = metavisit.metavisit_occurrence_id)
                    AND (
                            (event.concept_set_name LIKE
                              'diagnoses;kidney_disorder;%')
                         OR (event.concept_set_name LIKE
                              'diagnoses;exclusion;kidney;%')
                        )
                ))
       ORDER BY person_id,
                lower(metavisit_period)
      )
  SELECT person_id,
         metavisit_period,
         'hospitalization_without_kidney_disorder'::TEXT
    FROM _first_metavisit_without_diagnosis;

\echo '  listing competing risk #4, `censoring_event`'

-- death
INSERT INTO competing_risks
  SELECT op_start.person_id,
         codex_instant(event.death_date),
         'censoring_event'::TEXT
    FROM sequences_of_interest AS op_start
    JOIN {{cdm_schema}}.death AS event USING (person_id);

-- last recorded event
INSERT INTO competing_risks
  WITH
    _last_recorded_events AS (
         SELECT DISTINCT ON (op_start.person_id)
                op_start.person_id,
                event.event_period
           FROM sequences_of_interest AS op_start
           JOIN events_of_interest AS event USING (person_id)
       ORDER BY op_start.person_id,
                upper(event.event_period) DESC
      )
    SELECT person_id,
           codex_instant(upper(event_period) - 1),
           'censoring_event'::TEXT
      FROM _last_recorded_events AS event;

-- onset of excluded kidney condition
INSERT INTO competing_risks
  SELECT op_start.person_id,
         event.event_period,
         'censoring_event'::TEXT
    FROM sequences_of_interest AS op_start
    JOIN events_of_interest AS event USING (person_id)
   WHERE (event.concept_set_name LIKE
           'diagnoses;exclusion;kidney;%');

\echo '  indexing competing_risks'

\echo '    creating index on competing_risks.person_id'

CREATE INDEX ON competing_risks (person_id);

\echo '    creating index on competing_risks.event_period'

CREATE INDEX ON competing_risks USING GIST (event_period);

\echo '    creating index on competing_risks.event_type'

CREATE INDEX ON competing_risks (event_type);

\echo '  truncating competing_risks'

-- delete all competing risks that are not from patients in
-- `sequences_of_interest` (this should not be necessary)
DELETE
  FROM competing_risks AS op_stop
 USING (
           SELECT DISTINCT
                  op_stop.person_id
             FROM competing_risks AS op_stop
        LEFT JOIN sequences_of_interest AS op_start
            USING (person_id)
            WHERE (op_start.person_id IS NULL)
       ) AS orphan
 WHERE (op_stop.person_id = orphan.person_id);

-- delete all competing risks that started
-- on or before the index prescription date
DELETE
  FROM competing_risks AS op_stop
 USING sequences_of_interest AS op_start
 WHERE (op_start.person_id = op_stop.person_id)
   AND (lower(op_stop.event_period)
         <= op_start.period_3);

\echo '    analyzing competing_risks'

ANALYZE competing_risks;

/**
Test: each competing risk should have been found at least once

    SELECT event_type,
           count(DISTINCT person_id) AS n_persons
      FROM competing_risks
  GROUP BY event_type
  ORDER BY event_type;
**/
